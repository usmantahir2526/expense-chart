import { useState } from "react";
import Card from "./Card";
import ExpenseItem from "./ExpenseItem";
import ExpensesFilter from "./Expenses/ExpensesFilter";
import ExpensesList from "./Expenses/ExpensesList";
import ExpensesChart from "./Chart/ExpensesChart";

const Expenses = (props) => {

    const [selectedYear,setSelectedYear] = useState('2020');

    const getSelectedYear = (selectedValue) => {
      setSelectedYear(selectedValue);
    }

    const filterExpenses = props.expenses.filter(expense => {
      return expense.date.getFullYear().toString() === selectedYear;
    });

    return (  
      <Card>
        <ExpensesFilter onSelectedYear={getSelectedYear} selected={selectedYear}/>
        <ExpensesChart expenses={filterExpenses}></ExpensesChart>
        <ExpensesList items={filterExpenses}></ExpensesList>
      </Card>
    );
}

export default Expenses