import React, { useState } from 'react';

import ExpenseForm from './ExpenseForm';
import './NewExpense.css';

const NewExpense = (props) => {
const [isDisplay,setIsDisplay] = useState(false);

  const submitDataHandler = (expensesData) => {
    const expenses = {
      ...expensesData,
      id: Math.random().toString()
    };
    props.onAddExpense(expenses);
    setIsDisplay(false);
  }

  const formHandler = () => {
    setIsDisplay(true);
  }

  const cancelForm = () => {
    setIsDisplay(false);
  }

  return (
    <div className='new-expense'>
      {! isDisplay && <button onClick={formHandler}>Add New Expenses</button>}
      { isDisplay && <ExpenseForm cancelHandler={cancelForm} onSubmitData={submitDataHandler}/> }
    </div>
  );
};

export default NewExpense;