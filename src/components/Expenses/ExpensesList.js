import './ExpensesList.css';
import ExpenseItem from '../ExpenseItem';

const ExpensesList = (props) => {
    if( props.items.length === 0 ){
        return <ul className='expenses-list__fallback'>No Expenses Found.</ul>
    }
    
    return (
        <ul className='expenses-list'>
            {props.items.map((expense,index) => (<ExpenseItem
                key={index}
                title={expense.title} 
                amount={expense.amount} 
                date={expense.date}
            ></ExpenseItem>
            ))}
        </ul>
    )
}

export default ExpensesList